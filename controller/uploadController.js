import upload from '../multer_upload/multerUpload.js'

const uploadImage = (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.render('index', { msg: err })
    } else {
      if (req.file == undefined) {
        res.render('index', { msg: 'Please Choose some files' })
      } else {
        console.log(req.file)
        res.render('index', {
          msg: 'File upload success',
          file: `uploads/${req.file.filename}`,
        })
      }
    }
  })
}

export { uploadImage }
