import express from 'express'
import { uploadImage } from '../controller/uploadController.js'
const router = express.Router()

router.post('/', uploadImage)

export default router
