import express from 'express'
import ejs from 'ejs'
import path from 'path'
import uploadRouter from './routes/upload.js'

//Init app
const app = express()

//EJS
app.set('view engine', 'ejs')

//Path folder
app.use(express.static('./public'))

app.get('/', (req, res) => {
  res.render('index')
})
app.use('/upload', uploadRouter)

app.listen(3000, () => {
  console.log('Server Started at port 3000....')
})
